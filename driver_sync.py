#!/usr/bin/env python3
"""
Sync a GTFS-RT Enhanced JSON feed to Samsara.
"""
import asyncio
import json
import httpx
from samsara_auth import AUTH


def samsara_driver_from_operator(json):
    """Create a Samsara Driver from a GTFS-RT operator.

    We use the badge number as an external ID so that future updates
    don't need to look up the Samsara internal ID: we can use
    `badgeNo:XXXXXX` in place of that.

    Samsara doc: https://www.samsara.com/api#operation/createDriver
    """
    return {
        "externalIds": {"badgeNo": json["id"]},
        "name": json["name"],
        "password": "1234",
        "timezone": "America/New_York",
        "username": "{id}_{name}".format(**json).replace(" ", "_"),
    }


def handle_response(response):
    if response.status_code == 200:
        return None

    return response


async def create_samsara_driver(client, driver):
    """
    POST to Samsara API to create an address.
    """
    url = "https://api.samsara.com/fleet/drivers"
    response = await client.post(url, json=driver, auth=AUTH)
    response = handle_response(response)
    if not response:
        return response

    if (
        response.status_code == 400
        and response.json()["message"] == "Username is already taken."
    ):
        url = url + "/badgeNo:" + driver["externalIds"]["badgeNo"]
        response = await client.patch(url, json=driver, auth=AUTH)

    return handle_response(response)


def operators_from_json(json):
    for entity in json["entity"]:
        operator = entity["vehicle"].get("operator", {})
        if not operator.get("id"):
            continue
        yield operator


async def sync_drivers(client, drivers):
    for (driver, task) in [
        (driver, asyncio.create_task(create_samsara_driver(client, driver)))
        for driver in drivers
    ]:
        response = await task
        if response:
            print(driver)
            print(response)
            print(response.headers)
            print(response.json())


async def main():
    with open("tmp/VehiclePositions_enhanced.json") as fp:
        gtfs_rt = json.load(fp)
    operators = operators_from_json(gtfs_rt)
    drivers = [samsara_driver_from_operator(operator) for operator in operators]
    async with httpx.AsyncClient() as client:
        await sync_drivers(client, drivers)


if __name__ == "__main__":
    asyncio.run(main())
