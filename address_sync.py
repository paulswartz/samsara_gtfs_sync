#!/usr/bin/env python3
"""
Sync a GTFS stops.txt file to Samsara.
"""
import asyncio
import csv
import httpx
from samsara_auth import AUTH

ADDRESS_RADIUS_M = 5


def samsara_address_from_gtfs_stop(row):
    """
    Create a Samsara Address from a GTFS stop.

    Samsara doc: https://www.samsara.com/api#operation/createAddress
    """
    latitude = float(row["stop_lat"])
    longitude = float(row["stop_lon"])
    address = row["stop_address"]
    if address == "":
        description = row["stop_desc"] or row["stop_name"]
        address = f"{description}, {row['municipality']}, MA"
    return {
        "formattedAddress": address,
        "externalIds": {
            "gtfsId": f"stop_{row['stop_id']}".replace(" ", "_").replace("/", "_"),
        },
        "geofence": {
            "circle": {
                "latitude": latitude,
                "longitude": longitude,
                "radiusMeters": ADDRESS_RADIUS_M,
            }
        },
        "latitude": latitude,
        "longtiude": longitude,
        "name": f"{row['stop_name']} ({row['stop_id']})",
    }


def should_sync_gtfs_stop(row):
    """
    Boolean indicating whether the GTFS stop should be synced to Samsara.
    """
    return row["location_type"] == "0" and row["vehicle_type"] == "3"


def handle_response(response):
    if response.status_code == 200:
        return None

    return response


async def create_samsara_address(client, address):
    """
    POST to Samsara API to create an address.
    """
    url = "https://api.samsara.com/addresses"
    response = await client.post(url, json=address, auth=AUTH)
    response = handle_response(response)

    if not response:
        return response

    if response.json()["message"].startswith("Duplicate external id value"):
        url = url + "/gtfsId:" + address["externalIds"]["gtfsId"]
        response = await client.patch(url, json=address, auth=AUTH)

    return handle_response(response)


def gtfs_stops(fp):
    dr = csv.DictReader(fp)
    for row in dr:
        if not should_sync_gtfs_stop(row):
            continue
        yield row


async def main():
    with open("tmp/stops.txt") as fp:
        stops = gtfs_stops(fp)
        addresses = [samsara_address_from_gtfs_stop(row) for row in stops]
    async with httpx.AsyncClient() as client:
        tasks = [
            (address, asyncio.create_task(create_samsara_address(client, address)))
            for address in addresses
        ]
        for (address, task) in tasks:
            response = await task
            if not response:
                continue
            print(address)
            print(response)
            print(response.headers)
            print(response.json())


if __name__ == "__main__":
    asyncio.run(main())
