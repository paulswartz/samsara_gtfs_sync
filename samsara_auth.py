import os
from requests.auth import AuthBase


class SamsaraAuth(AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["Authorization"] = f"Bearer {self.token}"
        return r


AUTH = SamsaraAuth(os.environ["SAMSARA_API_TOKEN"])
